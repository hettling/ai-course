import os
import progressbar
import PIL
from PIL import Image

if __name__ == "__main__":
    dimensions = (500,500) # in pixels
    source_dir = "../data/source_data/images/original"
    target_dir = "../data/source_data/images/resized_500_w"

    images = os.listdir(source_dir)
    bar = progressbar.ProgressBar(len(images))
    bar.start()
    for i, file in enumerate(images):
        bar.update(i)
        source_file = os.path.join(source_dir, file)
        fname, ext = os.path.splitext(file)
        new_filename = fname + "_" + "500_w" + ext #.join(list(map(lambda x: str(x), dimensions))) + ext
        target_file = os.path.join(target_dir, new_filename)
        image = PIL.Image.open(source_file)
        #box = (0,0,image.size[0]/2.5, image.size[1]/1.1)
        #image_resized = image.resize(dimensions)#, PIL.Image.BOX, box=box)
        #image_resized.save(target_file)
        image.thumbnail(dimensions, Image.ANTIALIAS)
        image.save(target_file)