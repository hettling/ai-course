import os
import pandas
import numpy
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Dense, Input, LeakyReLU, concatenate, Lambda, Flatten, Conv2D, MaxPool2D, RNN, Layer, SimpleRNN, Dropout, GlobalMaxPooling2D
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, CSVLogger
from tensorflow.keras.optimizers import RMSprop
from sklearn.utils import shuffle
from sklearn.utils import resample
import sklearn.metrics as metrics

def get_model(output_shape):
    base_model = InceptionV3(weights='imagenet', include_top=False)
    last_full = base_model.output
    last_full = GlobalAveragePooling2D()(last_full)
    predictions = Dense(output_shape, activation='softmax')(last_full)
    model = Model(inputs=base_model.input, outputs=predictions)
    learning_rate = 1e-4
    model.compile(loss="categorical_crossentropy",optimizer=RMSprop(lr=learning_rate), metrics=["accuracy"])
    return model
    


if __name__ == "__main__":
    results_dir = "../results/016_training_min_10_images_testsplit_025_upsample_0.1/"
    weights_path = os.path.join(results_dir, "current_best_weights.hdf5")

    
    table = pandas.read_csv(os.path.join(results_dir, "table_train_input.csv"))
    
    model = get_model(len(set(table['class'])))
    model.load_weights(weights_path)
    
    datagen = ImageDataGenerator(rescale=1. / 255, validation_split=0.25)

    val_generator = datagen.flow_from_dataframe(table,
                                                target_size=(299, 299),
                                                x_col="image_loc", y_col="class", subset="validation", shuffle=False, seed=111, class_mode='categorical', interpolation="nearest", batch_size=32)
                                                #save_to_dir=save_dir_val)
    # res = model.evaluate(val_generator)

    print("Val generator: Number of classes: ", len(set(val_generator.classes)))
    #exit
    out = model.predict_generator(val_generator)
    #print("Output shape : ", out.shape)
    
    label_to_index = val_generator.class_indices
    index_to_label = dict(zip(label_to_index.values(), label_to_index.keys()))

    true_indices = val_generator.classes
    true_labels = [index_to_label[t] for t in true_indices]

    predict_indices = [numpy.argmax(o) for o in out]
    predict_labels = [index_to_label[t] for t in predict_indices]

    files = val_generator.filepaths
    
    # make results table
    results_df = pandas.DataFrame({"filename":files, "label_true":true_labels, "label_predicted":predict_labels, "index_true":true_indices, "index_predicted":predict_indices})

    # calculate per-class precision and recall
    per_class_results_df = pandas.DataFrame(columns=["taxname", "num_validation_images", "tp", "fp", "tn", "fn", "precision", "recall", "accuracy", "f1"])
    for taxname in list(set(true_labels)):
        tp = len(results_df.loc[(results_df.label_true==taxname) & (results_df.label_predicted==taxname)])
        fp = len(results_df.loc[(results_df.label_true!=taxname) & (results_df.label_predicted==taxname)])
        tn = len(results_df.loc[(results_df.label_true!=taxname) & (results_df.label_predicted!=taxname)])
        fn = len(results_df.loc[(results_df.label_true==taxname) & (results_df.label_predicted!=taxname)])
        precision = 0
        recall = 0
        f1 = 0
        if tp > 0:
            precision = tp / (tp + fp)
            recall = tp / (tp + fn)
        accuracy = (tp + tn) / (tp + tn + fp + fn)

        if precision > 0 and recall > 0:
            f1 = 2 * (precision * recall) / (precision + recall)
    
        per_class_results_df = per_class_results_df.append({"taxname" : taxname,
                                                            "num_validation_images" : tp + fn,
                                                            "tp" : tp,
                                                            "fp" : fp,
                                                            "tn" : tn,
                                                            "fn" : fn,
                                                            "precision" : precision,
                                                            "recall" : recall,
                                                            "accuracy" : accuracy,
                                                            "f1": f1}, ignore_index=True)
        
    per_class_results_df = per_class_results_df.sort_values("num_validation_images", ascending=False)

    # matrix = metrics.confusion_matrix(true_labels, predict_labels)
    # per class precision
    # matrix.diagonal()/matrix.sum(axis=0)

    # per class recall
    # matrix.diagonal()/matrix.sum(axis=1)
    print(metrics.classification_report(true_indices, predict_indices, digits=3))
    
    # save to file
    analysis_dir = os.path.join(results_dir, "analysis/")
    if not os.path.exists(analysis_dir):
        os.mkdir(analysis_dir)
    results_df.to_csv(os.path.join(analysis_dir, "results.csv"))
    per_class_results_df.to_csv(os.path.join(analysis_dir, "results_per_class.csv"))
