import pandas
import os
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


def read_image(path, size):
    im = Image.open(path)
    #im.thumbnail(size)
    im = im.resize(size)
    im = im.convert('L')
    im_arr = np.array(im)
    im.close()
    return im_arr


def get_dataset(spec, size):

    #spec = "Polyommatus icarus"
    #size = (128,128)
    
    table = pandas.read_csv("../data/source_data/blauwtjes/Blauwtjes.csv")
    images_dir = "/storage/hannes/git/ai-course/data/source_data/blauwtjes/images/"
    table["images_path"] = table.image_url.apply(lambda x: os.path.join(images_dir, x.split("/")[-1]))
    
    table = table.loc[table.taxon_full_name==spec]
    x = []
    for i, fname in enumerate(list(table['images_path'])[:1000]):
        print("Processing image # {}".format(i))
        try:
            xx = read_image(fname, size)
            x.append(xx)
        except Exception:
            pass
        #if x is None:
        #    x = xx
        #else:
        
    
     #x = np.array([read_image(fname, size) for fname in table["images_path"][:10] if os.path.exists(fname) and fname != "/storage/hannes/git/ai-course/data/source_data/blauwtjes/images/14972153.jpg"])

    return np.array(x)


