#!/bin/bash

# Convert xlsx tables to csv, we want the 'database' tab

counter=0
IFS=$(echo -en "\n\b")
for i in $(ls *.xlsx); do
	((counter=counter+1))
	mkdir tmp/
	cp $i tmp/
	ssconvert -S tmp/$i tmp/pm.csv
	filename="file-${counter}.csv"
	# XXX database tab not always the second one, will have to
	# adjust manually a bit
	cp tmp/pm.csv.1 $filename
	rm -rf tmp/
done
