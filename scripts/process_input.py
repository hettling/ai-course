import pandas

if __name__ == "__main__":
    table_all = None

    # File #7 does not need this processing, just the set name
    filename = "../data/source_data/csv/file-7.csv"
    t = pandas.read_csv(filename)
    t['set'] = "7"
    # t.to_csv("../data/source_data/cleaned/7.csv")
    table_all = t

    # process all other files
    for index in range(1,7):
        infile = "../data/source_data/csv/file-{}.csv".format(index)
        tt = pandas.read_csv(infile)
        tt['set'] = index
        #outfile = "../data/source_data/cleaned/{}.csv".format(index)
        #process_table(infile, outfile, str(index))
        ##tt = pandas.read_csv(outfile)
        table_all = table_all.append(tt)

    relevant_cols = ['Registration number', 'Author and year', 'sex', 'sexe', 'Identifier',
                     'Identification date', 'Current Collection Name', 'Basis of Record',
                     'Preserved Part', 'Present in Collection ', 'Is printed', 'Datagroup preferred',
                     'Determination preferred', 'Is partial determination', 'set', 'Prefix',
                     'CatalogNumber']
    table_all[relevant_cols].to_csv("../data/source_data/cleaned/all_new.csv", index=False)
