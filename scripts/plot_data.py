import pandas

if __name__ == "__main__":
    table = pandas.read_csv("../data/source_data/cleaned/papillotten_images_postprocessed_500_w.csv")
    table["genus"] = table["genus"].str.replace("?", "")
    table["genus"] = table["genus"].str.replace("cf.", "")
    table["genus"] = table["genus"].str.strip()

    # omit set 7
    table = table[table["set"]!=7]

    # omit the ones that do not have a genus
    table = table[table['genus'].notna()]


    df = table.groupby("genus").size().reset_index(name='counts').sort_values(["counts"], ascending=False)
    import matplotlib.pyplot as plt
    import numpy as np
    x = np.arange(len(df))
    fig, ax = plt.subplots()
    #ax.yaxis.set_major_formatter(formatter)
    plt.bar(x, df.counts)
    plt.xlabel("genus index")
    plt.ylabel("Number of images for genus")
    plt.title("Images per genus")

    gg = list(df.genus)
    gg.sort()