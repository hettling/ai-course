import pandas
import progressbar
import requests
import json
import threading
import urllib
from urllib.error import HTTPError
import os
from typing import List

def get_image_addresses(unitID:str) -> List[str]:
    '''
    Get image addresses for a unitID from the NBA
    :param unitID: unitID of specimen
    :return: List of image URLs
    '''

    results = {}
    results['urls'] = []
    results['identification'] = None
    # ask NBA for media content for a registration number
    url = "https://api.biodiversitydata.nl/v2/specimen/query/?unitID={}".format(unitID)
    res = requests.get(url)
    res.content
    res = requests.get(url)
    j = json.loads(res.content)
    assert len(j['resultSet']) < 2

    # get media uris
    if 'associatedMultiMediaUris' in j['resultSet'][0]['item']:
        media_uris = j['resultSet'][0]['item']['associatedMultiMediaUris']
        for m in media_uris:
            results['urls'].append(m['accessUri'])

    # also get identification
    # watch out, we assume there is only one identification!
    results['identification'] = j['resultSet'][0]['item']['identifications'][0]

    return results

def download_image(url:str, filename:str, target_dir:str) :
    '''
    success = True
    save_path = os.path.join(target_dir, filename)
    try:
        urllib.request.urlretrieve(url, save_path)
    except HTTPError:
        success = False
    '''
    success = True
    return success

if __name__ == "__main__":
    target_dir = "../data/source_data/images"
    table = pandas.read_csv("../data/source_data/cleaned/all_new.csv")
    # also store info per image
    image_table = pandas.DataFrame(columns=["image_uri"] + list(table.columns))
    # add taxonomic information from the NBA
    additional_cols = ['fullScientificName', 'taxonRank',
                       'order', 'subOrder', 'family', 'subFamily', 'tribe', 'genus', 'subgenus',
                       'specificEpithet', 'infraspecificEpithet']
    for col in additional_cols:
        table[col] = ""

    image_urls = []

    bar = progressbar.ProgressBar(len(table))
    bar.start()
    print("Retrieveing image URLs for {} unit IDs".format(len(table)))
    for i, row in table.iterrows():
        bar.update(i)
        regnr = row['Registration number']
        print('processing unitID', regnr)
        if pandas.isnull(regnr):
            print("Skipping registration number {}".format(regnr))
            continue
        results = get_image_addresses(regnr)
        # put taxonomy etc into the table
        table.iloc[i, table.columns.get_loc('fullScientificName')] = results['identification']['scientificName']['fullScientificName']
        table.iloc[i, table.columns.get_loc('taxonRank')] = results['identification']['taxonRank']
        for rank in ['order', 'subOrder', 'family', 'subFamily', 'tribe', 'genus', 'subgenus',
                       'specificEpithet', 'infraspecificEpithet']:
            if rank in results['identification']['defaultClassification']:
                table.iloc[i, table.columns.get_loc(rank)] = results['identification']['defaultClassification'][rank]

        urls = results['urls']
        for url in urls:
            s = pandas.Series(url, ['image_uri'])
            image_table = image_table.append(pandas.concat([s, table.iloc[i]]), ignore_index=True)
    table.to_csv("../data/source_data/cleaned/papillotten_observations_new.csv", index=False)
    image_table.to_csv("../data/source_data/cleaned/papillotten_images_new.csv", index=False)

    # get the images
    max_num_concurrent_threads = 70
    download_threads = []
    bar = progressbar.ProgressBar(len(image_table))
    bar.start()
    print("Downloading {} image URLs".format(len(image_table)))
    for i, url in enumerate(image_table.image_uri):
        bar.update(i)
        file_base = url.split("/")[-3]
        filename = file_base + ".jpg"

        thread = threading.Thread(target=download_image, args=(url, filename, target_dir))
        thread.start()
        download_threads.append(thread)

        if len(download_threads) > max_num_concurrent_threads:
            for thread in download_threads:
                thread.join()
            download_threads = []
