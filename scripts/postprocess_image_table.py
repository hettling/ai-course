import pandas
import glob
import os
import progressbar

''' 
    With this script we add two columns to the images table:
        * file name of the resized image
        * class: this will be Genus + specific epithet + infraspecific epithet
'''

if __name__ == "__main__":
    tab = pandas.read_csv("../data/source_data/cleaned/papillotten_images_new.csv")
    images_dir = "../data/source_data/images/resized/"

#    tab["specificEpithet"].isnull()
    
    image_locs = []
    classes = []
    
    bar = progressbar.ProgressBar(len(tab))
    bar.start()
    for i, row in tab.iterrows():
        # look for image path
        bar.update(i)
        file_base = row['image_uri'].split("/")[5]
        g = glob.glob(os.path.join(images_dir, tab.image_uri[i].split("/")[5] + "*"))
        assert(len(g)==1)
        image_locs.append(g[0])

        # concatenate ranks
        if not pandas.isnull(row['infraspecificEpithet']):
            cl = "{} {} {}".format(row["genus"], row["specificEpithet"], row["infraspecificEpithet"])
        else:
            cl = "{} {}".format(row["genus"], row["specificEpithet"])

        if pandas.isnull(row["specificEpithet"]):
            print("Regnr : {}".format(row["Registration number"]))
            print("Class : {}".format(cl))
            print("Set : {}".format(row["set"]))
        classes.append(cl)
                                
    tab['image_loc'] = image_locs
    tab['class'] = classes

    tab.to_csv("../data/source_data/cleaned/papillotten_images_postprocessed_cropped.csv")

