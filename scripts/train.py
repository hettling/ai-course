import pandas
import numpy
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Dense, Input, LeakyReLU, concatenate, Lambda, Flatten, Conv2D, MaxPool2D, RNN, Layer, SimpleRNN, Dropout, GlobalMaxPooling2D
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, CSVLogger
from tensorflow.keras.optimizers import RMSprop
from sklearn.utils import shuffle
from sklearn.utils import resample
from sklearn.model_selection import train_test_split


def get_imbalance_ratio(dataset, col_to_train):
    counts = dataset[col_to_train].value_counts()
    return min(counts) / max(counts)

def downsample(dataset, col_to_train, target_ratio, factor=0.9):
    while(get_imbalance_ratio(dataset, col_to_train) < target_ratio):
    
        counts = dataset[col_to_train].value_counts()
        majority_label = counts.idxmax()
        dataset_majority = dataset[dataset[col_to_train]==majority_label]
        dataset_other = dataset[dataset[col_to_train]!=majority_label]
        dataset_majority_downsampled = resample(dataset_majority,
                                                replace=False,
                                                n_samples=int(numpy.ceil(len(dataset_majority) * factor)),
                                                random_state=111)
        dataset = pandas.concat([dataset_majority_downsampled, dataset_other])
        print("downsample: dataset num rows : {}".format(len(dataset)))
    return dataset


def upsample(dataset, col_to_train, target_ratio, factor=1.1):
    while(get_imbalance_ratio(dataset, col_to_train) < target_ratio):
        counts = dataset[col_to_train].value_counts()
        minority_label = counts.idxmin()
        dataset_minority = dataset[dataset[col_to_train]==minority_label]
        dataset_other = dataset[dataset[col_to_train]!=minority_label]
        dataset_minority_upsampled = resample(dataset_minority,
                                                replace=True,
                                                n_samples=int(numpy.ceil(len(dataset_minority) * factor)),
                                                random_state=111)
        dataset = pandas.concat([dataset_minority_upsampled, dataset_other])
        print("upsample: dataset num rows : {}".format(len(dataset)))
    return dataset

# This upsampling is to avoid that classes are lost in the test split
# def upsample_for_split(dataset, col_to_train, test_split):
#    min_num_images = int(numpy.ceil(1/test_split))

    
    
    
if __name__ == "__main__":
    output_str = "016_training_min_10_images_testsplit_025_upsample_0.1"
    results_dir = "../results/" + output_str
    if not os.path.exists(results_dir):
        os.mkdir(results_dir)
    col_to_train = "class"

    images_dir = None#"../data/source_data/images/resized_500_w/"
    images_table = pandas.read_csv("../data/source_data/cleaned/papillotten_images_postprocessed_cropped.csv")
    
    # omit set 7
    images_table = images_table[images_table["set"]!=7]
    
    # omit rows with NAs in species or genus and uncertain genera
    images_table = images_table[images_table['genus'].notna()]
    images_table = images_table[~images_table.genus.str.contains("\?")]
    images_table = images_table[~images_table.genus.str.contains("cf.")]
    if col_to_train == "class":
        images_table = images_table[images_table['specificEpithet'].notna()]
        images_table = images_table[~images_table["class"].str.contains("\?")]
        images_table = images_table[~images_table["class"].str.contains("cf.")]

        
    images_table['image_loc'] = images_table.image_loc.str.replace("/home/ubuntu/", "/storage/")
    images_table['image_loc'] = images_table.image_loc.str.replace("../data/", "/storage/hannes/git/ai-course/data/")

    # omit classes that have less than 10 images    
    counts = images_table[col_to_train].value_counts()
    to_remove = counts.loc[counts<10].index
    images_table = images_table.loc[~images_table[col_to_train].isin(to_remove)]
    
    images_table = upsample(images_table, col_to_train, 0.1)
    images_table = shuffle(images_table, random_state=111)

    ratio = get_imbalance_ratio(images_table, col_to_train)
    print("Imbalance ratio : {}".format(ratio))
    
    # save images table to file
    images_table.to_csv(os.path.join(results_dir, "table_train_input.csv"))
    

    save_dir_train = os.path.join(results_dir, "/tmp_train")
    save_dir_val = os.path.join(results_dir, "/tmp_val")
    if not os.path.exists(save_dir_train): 
        os.mkdir(save_dir_train)
    if not os.path.exists(save_dir_val):
        os.mkdir(save_dir_val)
    

        
    # train_x, val_x, train_y, val_y = train_test_split(images_table['image_loc'], images_table['class'], test_size = 0.1, random_state = 111, stratify=images_table['class'])

        
    datagen = ImageDataGenerator(rescale=1. / 255, validation_split=0.25)
    
    train_generator = datagen.flow_from_dataframe(images_table,
                                                  shuffle=False,
                                                  target_size=(299,299),
                                                  x_col="image_loc",
                                                  y_col=col_to_train,
                                                  subset="training",
                                                  shear_range=0.2,
                                                  zoom_range=0.2,
                                                  horizontal_flip=True,
                                                  rotation_range=90,
                                                  save_to_dir=save_dir_train,
                                                  seed=111)

    #assert len(set(train_generator.classes))==len(set(images_table[col_to_train])), "Classes git lost in test split!!"
    #x    datagen = ImageDataGenerator(rescale=1. / 255, validation_split=0.1)

    val_generator = datagen.flow_from_dataframe(images_table,
                                                shuffle=False,
                                                target_size=(299, 299),
                                                x_col="image_loc",
                                                y_col=col_to_train,
                                                subset="validation",
                                                save_to_dir=save_dir_val, seed=111)
    
    #print("Val generator: Number of classes: ", len(set(val_generator.classes)))
    assert len(set(val_generator.classes))==len(set(images_table[col_to_train])), "Classes git lost in test split!!"
    
    input_shape = (299,299,3)
    output_shape = len(list(set(images_table[col_to_train])))#len(numpy.unique(train_generator.classes))

    base_model = InceptionV3(weights='imagenet', include_top=False)
    last_full = base_model.output
    last_full = GlobalAveragePooling2D()(last_full)
    predictions = Dense(output_shape, activation='softmax')(last_full)
    model = Model(inputs=base_model.input, outputs=predictions)

    learning_rate = 1e-4
    logdir = os.path.join(results_dir, "logs")
    if not os.path.exists(logdir):
        os.mkdir(logdir)

    tensorboard_callback = TensorBoard(log_dir=logdir)
    model_checkpoint_callback = ModelCheckpoint(
	filepath=os.path.join(results_dir, "current_best_weights.hdf5"),
        save_weights_only=True,
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)
    csv_logger_callback_stage_1 = CSVLogger(os.path.join(logdir, "epoch_metrics_stage_1.csv"))
    csv_logger_callback_stage_2 = CSVLogger(os.path.join(logdir, "epoch_metrics_stage_2.csv"))
    
    # Stage 1: train only randomly initialized top layers
    for layer in base_model.layers:
        layer.trainable = False
       
    model.compile(loss="categorical_crossentropy",optimizer=RMSprop(lr=learning_rate), metrics=["accuracy"])
    model.fit(train_generator, epochs=2, validation_data=val_generator, callbacks=[model_checkpoint_callback, csv_logger_callback_stage_1],
              workers=4)
    model.save_weights(os.path.join(results_dir, "weights_stage1.hdf5"))

    # Stage 2: train all layers
    for layer in model.layers:
        layer.trainable = True

    model.compile(loss="categorical_crossentropy",optimizer=RMSprop(lr=learning_rate), metrics=["accuracy"])
    model.fit(train_generator, epochs=30, validation_data=val_generator, callbacks=[model_checkpoint_callback, tensorboard_callback, csv_logger_callback_stage_2],
              workers=4)
    model.save_weights(os.path.join(results_dir, "weights_stage2.hdf5")) 
        
