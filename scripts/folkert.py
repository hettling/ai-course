import pandas
import numpy
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Dense, Input, LeakyReLU, concatenate, Lambda, Flatten, Conv2D, MaxPool2D, RNN, Layer, SimpleRNN, Dropout, GlobalMaxPooling2D
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, CSVLogger
from tensorflow.keras.optimizers import RMSprop
from sklearn.utils import shuffle
from sklearn.utils import resample
from sklearn.model_selection import train_test_split


if __name__ == "__main__":

    
    traindf = pandas.read_csv("/storage/hannes/git/ai-course/results/002_downsample_imbalance_ratio_0.001_noimages_dir/table_train_input.csv")
    COL_IMAGE = "image_loc"
    COL_CLASS = "class"
    

    datagen = ImageDataGenerator(
        rescale=1. / 255.,
        validation_split=0.1,
    )
    

    train_generator = datagen.flow_from_dataframe(
        dataframe=traindf,
        x_col=COL_IMAGE,
        y_col=COL_CLASS,
        class_mode="categorical",
        target_size=(299, 299),
        #batch_size=model_settings["batch_size"],
        interpolation="nearest",
        subset="training",
        shuffle=True,
        # save_to_dir= training_dir,

    )


    validation_generator = datagen.flow_from_dataframe(
        dataframe=traindf,
        x_col=COL_IMAGE,
        y_col=COL_CLASS,
        class_mode="categorical",
        target_size=(299, 299),
        #batch_size=model_settings["batch_size"],
        interpolation="nearest",
        subset="validation",
        shuffle=True,
        # save_to_dir= validation_dir,

    )


    input_shape = (299, 299, 3)

#    Inception V3
    base_model = InceptionV3(weights="imagenet", include_top=False, input_shape=input_shape)
    base_model.trainable = False
    model = Sequential()
    model.add(base_model)
    model.add(GlobalAveragePooling2D())
    model.add(Dropout(0.5))
    model.add(Dense(len(list(set(traindf["class"]))), activation='softmax'))
  

    model.compile(loss='categorical_crossentropy', #model_settings["loss"],
                 optimizer=RMSprop(lr=0.0001),
                #optimizer=SGD(lr=1e-4, momentum=0.9),
                 #  optimizer=keras.optimizers.Adadelta(),
                  #metrics=model_settings["metrics"] if "metrics" in model_settings else
                  #metrics=["acc", "loss", "val_acc", "val_loss"])
                  metrics=["accuracy"])
    model.summary()

    STEP_SIZE_TRAIN = train_generator.n // train_generator.batch_size
    STEP_SIZE_VALID = validation_generator.n // validation_generator.batch_size
    history = model.fit_generator(generator=train_generator,
                                  steps_per_epoch=STEP_SIZE_TRAIN,
                                  validation_data=validation_generator,
                                  validation_steps=STEP_SIZE_VALID,
                                  epochs=30)#,
                                  #callbacks=get_callback_list())
